===========================
Website Login Redirect Back
===========================

No special settings is needed, just install the module.

By default Odoo redirects portal users on `/my` portal dashboard after their success login.
This module make it redirect to the same page from where logging in process were initiated.
Say you are on the `/blog` page and click `Sing in` in the caption of the page to logging in.
After you log in you'll see the same `/blog` (or any other page you were at) page instead of `/my`.
