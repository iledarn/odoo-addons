from odoo import fields, models


class AuthOAuthProvider(models.Model):
    """Class defining the configuration values of an OAuth2 provider"""

    _inherit = "auth.oauth.provider"
    interoperable_id = fields.Many2one("auth.oauth.interoperable")


class AuthOAuthProviderInteroperable(models.Model):
    """Class defining OAuth2 providers interoperable"""

    _name = "auth.oauth.interoperable"
    _description = "Interoperable: providers with the same interoperable defined may replace each other based on common email values"
    # but be sure that each of them have email value validated
    _order = "name"

    name = fields.Char(string="Descriprion", required=True)  # e.g. Google + Facebook
    oauth_provider_ids = fields.One2many("auth.oauth.provider", "interoperable_id")
