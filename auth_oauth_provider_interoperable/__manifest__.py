{
    "name": "OAuth2 Authentication Provider Interoperable",
    "category": "Tools",
    "description": """
Configure selected OAuth2 Providers to be interoperable.
""",
    "website": "https://github.com/iledarn",
    "author": "iledarn",
    "license": "LGPL-3",
    "depends": ["auth_oauth"],
    "data": [
        "views/auth_oauth_views.xml",
        "views/res_config_settings_views.xml",
        "security/ir.model.access.csv",
    ],
}
