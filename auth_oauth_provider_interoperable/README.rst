Adds support for OAuth provider interoperables
==============================================

Configuration:
--------------
Create an Interoperable and specify providers that you think should work together (like Facebook and Google). You may do it from OAuth Providers form view or from special menu item.

Security Considerations:
------------------------
We are relying here on `email` field from providers. Be sure that your providers validates this field. Otherwise someone may use your email there and get access to you Odoo account.

Caveat: It works porperly only given that your users have the same email field in used providers

Here is how it works:
---------------------
TBD
